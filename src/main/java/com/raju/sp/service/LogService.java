package com.raju.sp.service;

import static com.raju.sp.common.constants.CommonConstants.R_ANALYTICS_TRAN_MGR;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.raju.sp.jpa.ra.entity.LogEntity;
import com.raju.sp.jpa.ra.repository.LogRepository;

@Service("logService")
public class LogService {
	private final LogRepository logRepository;

	@Autowired
	public LogService(LogRepository logRepository) {
		this.logRepository = logRepository;
	}

	@Transactional(value = R_ANALYTICS_TRAN_MGR)
	public void saveLog(LogEntity log) {
		logRepository.save(log);
	}
}
