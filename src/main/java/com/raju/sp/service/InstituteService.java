package com.raju.sp.service;

import static com.raju.sp.common.constants.CommonConstants.R_SYSTEM_TRAN_MGR;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.raju.sp.jpa.rs.entity.InstituteEntity;
import com.raju.sp.jpa.rs.repository.InstituteRepository;

@Service("instituteService")
public class InstituteService {

	private static final Logger logger = LoggerFactory.getLogger(InstituteService.class);

	private final InstituteRepository instituteRepository;

	@Autowired
	public InstituteService(InstituteRepository instituteRepository) {
		this.instituteRepository = instituteRepository;
	}

	@Transactional(readOnly = true, value = R_SYSTEM_TRAN_MGR)
	public List<InstituteEntity> findAllInstitutes() {
		logger.debug("Fetching all institutes ...");
		// instituteRepository.inOnlyTest("");
		return instituteRepository.findAll();
	}
}
