package com.raju.sp.service;

import static com.raju.sp.common.constants.CommonConstants.R_SYSTEM_TRAN_MGR;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.raju.sp.common.constants.enums.BranchYearTypeEnum;
import com.raju.sp.common.constants.enums.ExamTypeEnum;
import com.raju.sp.jpa.rs.entity.MarksEntity;
import com.raju.sp.jpa.rs.repository.MarksRepository;

@Service("resultService")
public class ResultService {

	private static final Logger logger = LoggerFactory.getLogger(ResultService.class);

	private final MarksRepository marksRepository;

	@Autowired
	public ResultService(MarksRepository marksRepository) {
		this.marksRepository = marksRepository;
	}

	@Transactional(readOnly = true, value = R_SYSTEM_TRAN_MGR)
	public List<MarksEntity> getResultForStudent(Long userId, Integer year, String branchYearType, String examType) {
		logger.debug("Getting result for student {0} for exam {1},{2},{3}", userId, branchYearType, year, examType);
		List<MarksEntity> marksList = marksRepository.getResultForStudent(userId, year,
				BranchYearTypeEnum.fromString(branchYearType), ExamTypeEnum.fromString(examType));
		return marksList;
	}

}
