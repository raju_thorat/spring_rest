package com.raju.sp.service;

import static com.raju.sp.common.constants.CommonConstants.R_SYSTEM_TRAN_MGR;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.raju.sp.jpa.rs.entity.BranchEntity;
import com.raju.sp.jpa.rs.repository.BranchRepository;

@Service("branchService")
public class BranchService {

	private static final Logger logger = LoggerFactory.getLogger(BranchService.class);

	private final BranchRepository branchRepository;

	@Autowired
	public BranchService(BranchRepository branchRepository) {
		this.branchRepository = branchRepository;
	}

	@Transactional(readOnly = true, value = R_SYSTEM_TRAN_MGR)
	public List<BranchEntity> findAllBranchs() {
		logger.debug("Fetching all branchs ...");
		// branchRepository.inOnlyTest("");
		return branchRepository.findAll();
	}
}
