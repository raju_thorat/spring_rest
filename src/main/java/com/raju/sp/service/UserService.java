package com.raju.sp.service;

import static com.raju.sp.common.constants.CommonConstants.R_SYSTEM_TRAN_MGR;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.raju.sp.jpa.dto.UserSearchResultDTO;
import com.raju.sp.jpa.rs.entity.UserEntity;
import com.raju.sp.jpa.rs.repository.UserRepository;

@Service("userService")
public class UserService {

	private static final Logger logger = LoggerFactory.getLogger(UserService.class);

	private final UserRepository userRepository;

	@Autowired
	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Transactional(readOnly = true, value = R_SYSTEM_TRAN_MGR)
	public List<UserEntity> findAllUsers() {
		// userRepository.inOnlyTest("");
		return userRepository.findAll();
	}

	@Transactional(readOnly = true, value = R_SYSTEM_TRAN_MGR)
	public UserEntity findById(Long id) {
		Optional<UserEntity> optUser = userRepository.findById(id);
		if (optUser.isPresent()) {
			return optUser.get();
		} else {
			return null;
		}
	}

	@Transactional(readOnly = true, value = R_SYSTEM_TRAN_MGR)
	public UserEntity findByEmailIdAndPassword(String emailId, String password) {
		return userRepository.findByEmailIdAndPassword(emailId, password);
	}

	@Transactional(readOnly = true, value = R_SYSTEM_TRAN_MGR)
	public UserEntity findByName(String name) {
		return null;
	}

	@Transactional(readOnly = true, value = R_SYSTEM_TRAN_MGR)
	public List<UserSearchResultDTO> findByCustomSearchTerm(String emailId) {
		return userRepository.findByCustomSearchTerm(emailId);
	}

	@Transactional(value = R_SYSTEM_TRAN_MGR)
	public void saveUser(UserEntity user) {
		logger.info("Adding new user : {}", user);
		userRepository.save(user);
	}

	/**
	 * JPA will update managed user entity. You are free to call
	 * userRepository.save() but it will trigger merge() which will be far more
	 * expensive than below code as merge() will fetch lazy as well as non-lazy
	 * collections. Simplest way is to use update query.
	 * 
	 * @param user
	 */
	@Transactional(value = R_SYSTEM_TRAN_MGR)
	public UserEntity updateUser(UserEntity user) {
		UserEntity currentUser = findById(user.getId());
		currentUser.setFirstName(user.getFirstName());
		currentUser.setLastName(user.getLastName());
		currentUser.setMiddleName(user.getMiddleName());
		currentUser.setPassword(user.getPassword());
		return currentUser;
	}

	@Transactional(readOnly = true, value = R_SYSTEM_TRAN_MGR)
	public void deleteUserById(long id) {
		// userRepository.delete(deleted);
	}

}
