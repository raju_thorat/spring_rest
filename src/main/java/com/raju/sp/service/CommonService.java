package com.raju.sp.service;

import static com.raju.sp.common.constants.CommonConstants.R_SYSTEM_TRAN_MGR;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.raju.sp.common.constants.enums.BranchYearTypeEnum;
import com.raju.sp.common.constants.enums.ExamTypeEnum;
import com.raju.sp.common.constants.enums.GenderEnum;
import com.raju.sp.common.constants.enums.UserTypeEnum;
import com.raju.sp.jpa.rs.entity.BranchEntity;
import com.raju.sp.jpa.rs.entity.BranchYearEntity;
import com.raju.sp.jpa.rs.entity.ExamEntity;
import com.raju.sp.jpa.rs.entity.InstituteEntity;
import com.raju.sp.jpa.rs.entity.MarksEntity;
import com.raju.sp.jpa.rs.entity.SubjectEntity;
import com.raju.sp.jpa.rs.entity.UserEntity;
import com.raju.sp.jpa.rs.repository.BranchRepository;
import com.raju.sp.jpa.rs.repository.BranchYearRepository;
import com.raju.sp.jpa.rs.repository.ExamRepository;
import com.raju.sp.jpa.rs.repository.InstituteRepository;
import com.raju.sp.jpa.rs.repository.MarksRepository;
import com.raju.sp.jpa.rs.repository.SubjectRepository;
import com.raju.sp.jpa.rs.repository.UserRepository;

@Service("commonService")
public class CommonService {

	private static final Logger logger = LoggerFactory.getLogger(CommonService.class);

	private final UserRepository userRepository;
	private final InstituteRepository instituteRepository;
	private final BranchRepository branchRepository;
	private final SubjectRepository subjectRepository;
	private final MarksRepository marksRepository;
	private final BranchYearRepository branchYearRepository;
	private final ExamRepository examRepository;

	@Autowired
	public CommonService(UserRepository userRepository, InstituteRepository instituteRepository,
			BranchRepository branchRepository, SubjectRepository subjectRepository, MarksRepository marksRepository,
			BranchYearRepository branchYearRepository, ExamRepository examRepository) {
		this.userRepository = userRepository;
		this.instituteRepository = instituteRepository;
		this.branchRepository = branchRepository;
		this.subjectRepository = subjectRepository;
		this.marksRepository = marksRepository;
		this.branchYearRepository = branchYearRepository;
		this.examRepository = examRepository;
	}

	@Transactional(value = R_SYSTEM_TRAN_MGR)
	public void addInitialTestData() {
		logger.debug("Adding test data.");
		InstituteEntity instituteFergusson = new InstituteEntity.InstituteEntityBuilder(null)
				.setName("Fergsson College").build();

		BranchEntity branchFcCs = new BranchEntity.BranchEntityBuilder(null).setInstituteEntity(instituteFergusson)
				.setName("Computer Science").build();

		UserEntity userRajendra = new UserEntity.UserEntityBuilder(null).setRollNumber(1)
				.setEmailId("rajendrat@xpanxion.co.in").setPassword("1234").setFirstName("Rajendra").setLastName("Thorat")
				.setUserType(UserTypeEnum.STUDENT).setBranch(branchFcCs).setGender(GenderEnum.MALE).build();

		UserEntity userVivek = new UserEntity.UserEntityBuilder(null).setRollNumber(2)
				.setEmailId("vivekk@xpanxion.co.in").setPassword("1234").setFirstName("Vivek").setLastName("Kumar")
				.setUserType(UserTypeEnum.STUDENT).setBranch(branchFcCs).setGender(GenderEnum.MALE).build();

		UserEntity userPrashant = new UserEntity.UserEntityBuilder(null).setRollNumber(3)
				.setEmailId("prashantknalawade@xpanxion.co.in").setPassword("1234").setFirstName("Prashant")
				.setLastName("Nalawade").setUserType(UserTypeEnum.STUDENT).setBranch(branchFcCs)
				.setGender(GenderEnum.MALE).build();

		UserEntity userPoonam = new UserEntity.UserEntityBuilder(null).setRollNumber(4)
				.setEmailId("poonamc@xpanxion.co.in").setPassword("1234").setFirstName("Poonam").setLastName("Chuahan")
				.setUserType(UserTypeEnum.STUDENT).setBranch(branchFcCs).setGender(GenderEnum.FEMALE).build();

		UserEntity userPriti = new UserEntity.UserEntityBuilder(null).setRollNumber(5)
				.setEmailId("pritit@xpanxion.co.in").setPassword("1234").setFirstName("Priti").setLastName("Tiwari")
				.setUserType(UserTypeEnum.STUDENT).setBranch(branchFcCs).setGender(GenderEnum.FEMALE).build();

		SubjectEntity subjectMath = new SubjectEntity.SubjectEntityBuilder(null).setBranch(branchFcCs).setName("Math")
				.build();

		SubjectEntity subjectPhysics = new SubjectEntity.SubjectEntityBuilder(null).setBranch(branchFcCs)
				.setName("Physics").build();

		SubjectEntity subjectChemistry = new SubjectEntity.SubjectEntityBuilder(null).setBranch(branchFcCs)
				.setName("Chemistry").build();

		SubjectEntity subjectBiology = new SubjectEntity.SubjectEntityBuilder(null).setBranch(branchFcCs)
				.setName("Biology").build();

		instituteRepository.save(instituteFergusson);
		branchRepository.save(branchFcCs);
		userRepository.save(userRajendra);
		userRepository.save(userVivek);
		userRepository.save(userPrashant);
		userRepository.save(userPoonam);
		userRepository.save(userPriti);
		subjectRepository.save(subjectMath);
		subjectRepository.save(subjectPhysics);
		subjectRepository.save(subjectChemistry);
		subjectRepository.save(subjectBiology);

		BranchYearEntity branchYear1stCS = new BranchYearEntity.BranchYearEntityBuilder(null)
				.setBranchEntity(branchFcCs).setBranchYearType(BranchYearTypeEnum.FRIST_YEAR).setYear(2018).build();
		branchYearRepository.save(branchYear1stCS);

		ExamEntity examEntity = new ExamEntity.ExamEntityBuilder(null).setBranchYear(branchYear1stCS)
				.setExamType(ExamTypeEnum.SEMISTER).build();
		examRepository.save(examEntity);

		List<MarksEntity> rajendraMarks = new ArrayList<>(4);
		rajendraMarks.add(new MarksEntity.MarksEntityBuilder(null, examEntity, subjectMath, 80, userRajendra).build());
		rajendraMarks
				.add(new MarksEntity.MarksEntityBuilder(null, examEntity, subjectPhysics, 70, userRajendra).build());
		rajendraMarks
				.add(new MarksEntity.MarksEntityBuilder(null, examEntity, subjectChemistry, 52, userRajendra).build());
		rajendraMarks
				.add(new MarksEntity.MarksEntityBuilder(null, examEntity, subjectBiology, 90, userRajendra).build());
		marksRepository.saveAll(rajendraMarks);

		List<MarksEntity> vivekMarks = new ArrayList<>(4);
		vivekMarks.add(new MarksEntity.MarksEntityBuilder(null, examEntity, subjectMath, 81, userVivek).build());
		vivekMarks.add(new MarksEntity.MarksEntityBuilder(null, examEntity, subjectPhysics, 71, userVivek).build());
		vivekMarks.add(new MarksEntity.MarksEntityBuilder(null, examEntity, subjectChemistry, 53, userVivek).build());
		vivekMarks.add(new MarksEntity.MarksEntityBuilder(null, examEntity, subjectBiology, 91, userVivek).build());
		marksRepository.saveAll(vivekMarks);

		List<MarksEntity> prashantMarks = new ArrayList<>(4);
		prashantMarks.add(new MarksEntity.MarksEntityBuilder(null, examEntity, subjectMath, 82, userPrashant).build());
		prashantMarks
				.add(new MarksEntity.MarksEntityBuilder(null, examEntity, subjectPhysics, 72, userPrashant).build());
		prashantMarks
				.add(new MarksEntity.MarksEntityBuilder(null, examEntity, subjectChemistry, 54, userPrashant).build());
		prashantMarks
				.add(new MarksEntity.MarksEntityBuilder(null, examEntity, subjectBiology, 92, userPrashant).build());
		marksRepository.saveAll(prashantMarks);

		List<MarksEntity> poonamMarks = new ArrayList<>(4);
		poonamMarks.add(new MarksEntity.MarksEntityBuilder(null, examEntity, subjectMath, 83, userPoonam).build());
		poonamMarks.add(new MarksEntity.MarksEntityBuilder(null, examEntity, subjectPhysics, 73, userPoonam).build());
		poonamMarks.add(new MarksEntity.MarksEntityBuilder(null, examEntity, subjectChemistry, 55, userPoonam).build());
		poonamMarks.add(new MarksEntity.MarksEntityBuilder(null, examEntity, subjectBiology, 93, userPoonam).build());
		marksRepository.saveAll(poonamMarks);

		List<MarksEntity> pritiMarks = new ArrayList<>(4);
		pritiMarks.add(new MarksEntity.MarksEntityBuilder(null, examEntity, subjectMath, 84, userPriti).build());
		pritiMarks.add(new MarksEntity.MarksEntityBuilder(null, examEntity, subjectPhysics, 74, userPriti).build());
		pritiMarks.add(new MarksEntity.MarksEntityBuilder(null, examEntity, subjectChemistry, 56, userPriti).build());
		pritiMarks.add(new MarksEntity.MarksEntityBuilder(null, examEntity, subjectBiology, 94, userPriti).build());
		marksRepository.saveAll(pritiMarks);

	}
}
