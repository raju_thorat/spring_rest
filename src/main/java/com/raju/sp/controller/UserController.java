package com.raju.sp.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.raju.sp.exeptionhandling.NoDataFoundException;
import com.raju.sp.exeptionhandling.ResourceNotFoundException;
import com.raju.sp.jpa.dto.UserSearchResultDTO;
import com.raju.sp.jpa.ra.entity.LogEntity;
import com.raju.sp.jpa.rs.entity.BranchEntity;
import com.raju.sp.jpa.rs.entity.UserEntity;
import com.raju.sp.service.LogService;
import com.raju.sp.service.UserService;

@RestController
@CrossOrigin
// @RequestMapping(value = "/users")
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	private final UserService userService;
	private final LogService logService;

	@Autowired
	public UserController(UserService userService, LogService logService) {
		this.userService = userService;
		this.logService = logService;
	}

	/**
	 * @return All the users in the system
	 */
	// @PreAuthorize("hasAuthority('ROLE_USER')")
	@RequestMapping(value = "/users", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAllUsers() {
		logger.debug("Getting all users...");

		List<UserEntity> users = userService.findAllUsers();
		if (users.isEmpty()) {
			logger.debug("No users found in the system...");
			throw new NoDataFoundException("No users found in the system.");
		}
		// The ResponseEntity is a class in Spring MVC that acts as a wrapper
		// for an object to be used as the body of the result together with a
		// HTTP status code.
		return new ResponseEntity<List<UserEntity>>(users, HttpStatus.OK);
	}

	// -------------------Retrieve Single

	@RequestMapping(value = "/users/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserEntity> getUser(@PathVariable("id") Long id) {
		logger.debug("Fetching User with id {}", id);
		UserEntity user = userService.findById(id);
		if (user == null) {
			logger.debug("User with id {} not found.", id);
			throw new ResourceNotFoundException("User with id " + id + " not found.");
		}
		return new ResponseEntity<UserEntity>(user, HttpStatus.OK);
	}

	@RequestMapping(value = "/getUserByEmail/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<UserSearchResultDTO>> getUserByEmail(@PathVariable("emailId") String emailId) {
		logger.debug("Fetching User with email {}", emailId);
		List<UserSearchResultDTO> userSearchResultDTOList = userService.findByCustomSearchTerm(emailId);
		if (userSearchResultDTOList.isEmpty()) {
			return new ResponseEntity<List<UserSearchResultDTO>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<UserSearchResultDTO>>(userSearchResultDTOList, HttpStatus.OK);
	}

	@RequestMapping(value = "/validateLogin", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserEntity> validateLogin(@RequestBody UserEntity user) {
		logger.debug("Fetching User with email {}", user.getEmailId());
		UserEntity userEntity = userService.findByEmailIdAndPassword(user.getEmailId(), user.getPassword());
		if (userEntity == null) {
			logger.debug("User with emailId {} not found.", user.getEmailId());
			return new ResponseEntity<UserEntity>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<UserEntity>(userEntity, HttpStatus.OK);
	}

	/**
	 * Create new user
	 * 
	 * @param user
	 * @param ucBuilder
	 * @return
	 */
	@RequestMapping(value = "/users", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createUser(@RequestBody UserEntity user, UriComponentsBuilder ucBuilder) {
		BranchEntity branch = new BranchEntity();
		branch.setId(1L);
		user.setBranch(branch);
		userService.saveUser(user);
		logService.saveLog(new LogEntity(null, "Adding user : " + user.getEmailId()));
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	// ------------------- Update a User
	// --------------------------------------------------------

	@RequestMapping(value = "/updateOneUser/{id}", method = RequestMethod.PUT)
	public ResponseEntity<UserEntity> updateUser(@PathVariable("id") Long id, @RequestBody UserEntity user) {
		user = userService.updateUser(user);
		return new ResponseEntity<UserEntity>(user, HttpStatus.OK);
	}

	@RequestMapping(value = "/deleteOneUser/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Long> deleteOneUser(@PathVariable("id") Long id, @RequestBody UserEntity user) {
		userService.deleteUserById(id);
		return new ResponseEntity<Long>(id, HttpStatus.OK);
	}

}