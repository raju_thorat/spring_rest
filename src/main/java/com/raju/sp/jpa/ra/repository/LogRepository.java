package com.raju.sp.jpa.ra.repository;

import java.io.Serializable;

import org.springframework.data.repository.CrudRepository;

import com.raju.sp.jpa.ra.entity.LogEntity;

public interface LogRepository extends CrudRepository<LogEntity, Serializable> {

}
