package com.raju.sp.jpa.rs.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author raju
 */
@Entity
// unique constraint must have database column names
@Table(name = "subject", uniqueConstraints = @UniqueConstraint(columnNames = { "BRANCH_ID", "NAME" }))
public class SubjectEntity implements Serializable, Comparable<SubjectEntity> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Basic(optional = false)
	@Column(name = "ID")
	private Long id;

	@Basic(optional = false)
	@Column(name = "NAME", nullable = false)
	private String name;

	@Basic(optional = false)
	@ManyToOne
	@JoinColumn(name = "BRANCH_ID", referencedColumnName = "ID", nullable = false)
	private BranchEntity branch;

	public SubjectEntity() {
		super();
	}

	private SubjectEntity(SubjectEntityBuilder builder) {
		this.id = builder.id;
		this.name = builder.name;
		this.branch = builder.branch;

	}

	public static class SubjectEntityBuilder {

		private Long id;
		private String name;
		private BranchEntity branch;

		public SubjectEntityBuilder(Long id) {
			this.id = id;
		}

		public SubjectEntityBuilder setName(String name) {
			this.name = name;
			return this;
		}

		public SubjectEntityBuilder setBranch(BranchEntity branch) {
			this.branch = branch;
			return this;
		}

		public SubjectEntity build() {
			return new SubjectEntity(this);
		}
	}

	@Override
	public int compareTo(SubjectEntity o) {
		int bCodeComp = this.getBranch().getName().compareTo(o.getBranch().getName());
		if (bCodeComp != 0) {
			return bCodeComp;
		} else {
			return this.name.compareTo(o.getName());
		}
	}

	public SubjectEntity(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BranchEntity getBranch() {
		return branch;
	}

	public void setBranch(BranchEntity branch) {
		this.branch = branch;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
