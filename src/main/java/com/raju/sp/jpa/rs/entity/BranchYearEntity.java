package com.raju.sp.jpa.rs.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.raju.sp.common.constants.enums.BranchYearTypeEnum;

/**
 * Since many activities can related to year and batch, so not adding this data
 * in exam table
 * 
 * @author rajendrat
 *
 */
@Entity
@Table(name = "BRANCH_YEAR")
public class BranchYearEntity implements Serializable, Comparable<BranchYearEntity> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Basic(optional = false)
	@Column(name = "ID")
	private Long id;

	@Basic(optional = false)
	@Column(name = "YEAR", nullable = false)
	private Integer year;

	@Basic(optional = false)
	@Enumerated(EnumType.STRING)
	@Column(name = "BRANCH_YEAR_TYPE", nullable = false)
	private BranchYearTypeEnum branchYearType;

	@Basic(optional = false)
	@ManyToOne
	@JoinColumn(name = "BRANCH_ID", referencedColumnName = "ID", nullable = false)
	private BranchEntity branch;

	public BranchYearEntity() {
		super();
	}

	private BranchYearEntity(BranchYearEntityBuilder builder) {
		this.id = builder.id;
		this.year = builder.year;
		this.branchYearType = builder.branchYearType;
		this.branch = builder.branch;
	}

	public static class BranchYearEntityBuilder {
		private Long id;
		private Integer year;
		private BranchYearTypeEnum branchYearType;
		private BranchEntity branch;

		public BranchYearEntityBuilder(Long id) {
			this.id = id;
		}

		public BranchYearEntity build() {
			return new BranchYearEntity(this);
		}

		public BranchYearEntityBuilder setYear(Integer year) {
			this.year = year;
			return this;
		}

		public BranchYearEntityBuilder setBranchYearType(BranchYearTypeEnum branchYearType) {
			this.branchYearType = branchYearType;
			return this;
		}

		public BranchYearEntityBuilder setBranchEntity(BranchEntity branch) {
			this.branch = branch;
			return this;
		}
	}

	@Override
	public int compareTo(BranchYearEntity o) {
		return this.id.compareTo(o.id);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public BranchEntity getBranch() {
		return branch;
	}

	public void setBranch(BranchEntity branch) {
		this.branch = branch;
	}

	public BranchYearTypeEnum getBranchYearType() {
		return branchYearType;
	}

	public void setBranchYearType(BranchYearTypeEnum branchYearType) {
		this.branchYearType = branchYearType;
	}

}
