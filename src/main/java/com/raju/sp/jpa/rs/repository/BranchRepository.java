package com.raju.sp.jpa.rs.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.raju.sp.jpa.rs.entity.BranchEntity;

public interface BranchRepository extends JpaRepository<BranchEntity, Long> {
}