package com.raju.sp.jpa.rs.repository.custom;

import static com.raju.sp.common.constants.CommonConstants.R_SYSTEM_EN_MGR_FACTORY;
import static com.raju.sp.common.constants.CommonConstants.R_SYSTEM_JDBC_TEMPLATE;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.raju.sp.jpa.dto.UserSearchResultDTO;

//#$#$ Name must be UserRepositoryImpl if main repository is UserRepository
//Otherwise spring data will try generate query method for findByCustomSearchTerm
/**
 * You create any type of custom objects using this class. Just follow the naming convention
 * 
 * @author raju
 *
 */
@Repository
public final class UserRepositoryImpl implements CustomUserRepository {

	private static final String SEARCH_UserEntity_ENTRIES = "SELECT u.id, u.city FROM user u WHERE u.email=:email";

	private final NamedParameterJdbcTemplate jdbcTemplate;

	@PersistenceContext(unitName = R_SYSTEM_EN_MGR_FACTORY)
	private EntityManager em;

	@Override
	public void inOnlyTest(String inParam1) {
		this.em.createNativeQuery("BEGIN in_only_test(:inParam1); END;").setParameter("inParam1", inParam1).executeUpdate();
	}

	@Autowired
	public UserRepositoryImpl(@Qualifier(R_SYSTEM_JDBC_TEMPLATE) NamedParameterJdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	// @Transactional(readOnly = true)
	@Override
	public List<UserSearchResultDTO> findByCustomSearchTerm(String searchTerm) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("email", searchTerm);

		List<UserSearchResultDTO> searchResults = jdbcTemplate.query(SEARCH_UserEntity_ENTRIES, queryParams,
				new BeanPropertyRowMapper<>(UserSearchResultDTO.class));

		return searchResults;
	}
}