package com.raju.sp.jpa.rs.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.raju.sp.jpa.rs.entity.ExamEntity;

public interface ExamRepository extends JpaRepository<ExamEntity, Long> {
}