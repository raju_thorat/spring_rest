package com.raju.sp.jpa.rs.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.raju.sp.jpa.rs.entity.UserEntity;
import com.raju.sp.jpa.rs.repository.custom.CustomUserRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long>, CustomUserRepository {

	Optional<UserEntity> findById(Long id);

	UserEntity findByEmailId(String emailId);

	UserEntity findByEmailIdAndPassword(String emailId, String password);
}