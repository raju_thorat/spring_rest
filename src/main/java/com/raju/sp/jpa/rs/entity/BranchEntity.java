package com.raju.sp.jpa.rs.entity;

import java.io.Serializable;
import java.util.SortedSet;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "branch", uniqueConstraints = @UniqueConstraint(columnNames = { "INSTITUTE_ID", "NAME" }))
public class BranchEntity implements Serializable, Comparable<BranchEntity> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Basic(optional = false)
	@Column(name = "ID")
	private Long id;

	@Basic(optional = false)
	@Column(name = "NAME", nullable = false)
	private String name;

	@Basic(optional = false)
	@ManyToOne
	@JoinColumn(name = "INSTITUTE_ID", nullable = false)
	private InstituteEntity institute;

	@OrderBy("id,name")
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "branch")
	private SortedSet<SubjectEntity> subjects;

	@OrderBy("id,name")
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "branch")
	private SortedSet<UserEntity> students;

	@OrderBy("id,name")
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "branch")
	private SortedSet<UserEntity> teachers;

	public BranchEntity() {
		super();
	}

	private BranchEntity(BranchEntityBuilder builder) {
		this.id = builder.id;
		this.name = builder.name;
		this.institute = builder.institute;
		this.subjects = builder.subjects;
	}

	public static class BranchEntityBuilder {
		private Long id;
		private String name;
		private InstituteEntity institute;
		private SortedSet<SubjectEntity> subjects;

		public BranchEntityBuilder(Long id) {
			this.id = id;
		}

		public BranchEntityBuilder setName(String name) {
			this.name = name;
			return this;
		}

		public BranchEntityBuilder setInstituteEntity(InstituteEntity institute) {
			this.institute = institute;
			return this;
		}

		public BranchEntityBuilder setSubjectSet(SortedSet<SubjectEntity> subjects) {
			this.subjects = subjects;
			return this;
		}

		public BranchEntity build() {
			return new BranchEntity(this);
		}

	}

	@Override
	public int compareTo(BranchEntity o) {
		return this.name.compareTo(o.name);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public InstituteEntity getInstitute() {
		return institute;
	}

	public void setInstitute(InstituteEntity institute) {
		this.institute = institute;
	}

	public SortedSet<UserEntity> getStudents() {
		return students;
	}

	public void setStudents(SortedSet<UserEntity> students) {
		this.students = students;
	}

	public SortedSet<UserEntity> getTeachers() {
		return teachers;
	}

	public void setTeachers(SortedSet<UserEntity> teachers) {
		this.teachers = teachers;
	}

	public SortedSet<SubjectEntity> getSubjects() {
		return subjects;
	}

	public void setSubjects(SortedSet<SubjectEntity> subjects) {
		this.subjects = subjects;
	}

}
