package com.raju.sp.jpa.rs.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.raju.sp.common.constants.enums.GenderEnum;
import com.raju.sp.common.constants.enums.UserTypeEnum;

@Entity
@Table(name = "user")
public class UserEntity implements Serializable, Comparable<UserEntity> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Basic(optional = false)
	@Column(name = "ID")
	private Long id;

	@Basic(optional = false)
	@Column(name = "ROLL_NUMBER", nullable = false)
	private Integer rollNumber;

	@Basic(optional = false)
	@Column(name = "EMAIL_ID", unique = true, nullable = false)
	private String emailId;

	@Basic(optional = false)
	@Column(name = "PASSWORD", length = 60, nullable = false)
	private String password;

	@Basic(optional = false)
	@Column(name = "FIRST_NAME", nullable = false)
	private String firstName;

	@Basic(optional = false)
	@Column(name = "LAST_NAME", nullable = false)
	private String lastName;

	@Column(name = "MIDDLE_NAME")
	private String middleName;

	@Basic(optional = false)
	@Enumerated(EnumType.STRING)
	@Column(name = "GENDER", nullable = false)
	private GenderEnum gender;

	@Basic(optional = false)
	@Enumerated(EnumType.STRING)
	@Column(name = "USER_TYPE", nullable = false)
	private UserTypeEnum userType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "BIRTH_DATE")
	private Date birthDate;

	@Basic(optional = false)
	@ManyToOne
	@JoinColumn(name = "BRANCH_ID", referencedColumnName = "ID", nullable = false)
	private BranchEntity branch;

	@ManyToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.DETACH })
	@JoinTable(name = "user_subject", joinColumns = { @JoinColumn(name = "USER_ID") }, inverseJoinColumns = { @JoinColumn(name = "SUBJECT_ID") })
	private List<SubjectEntity> subjects;

	public UserEntity() {
		super();
	}

	private UserEntity(UserEntityBuilder builder) {
		this.id = builder.id;
		this.emailId = builder.emailId;
		this.password = builder.password;
		this.firstName = builder.firstName;
		this.lastName = builder.lastName;
		this.userType = builder.userType;
		this.branch = builder.branch;
		this.middleName = builder.middleName;
		this.gender = builder.gender;
		this.birthDate = builder.birthDate;
		this.subjects = builder.subjects;
		this.rollNumber = builder.rollNumber;
	}

	public static class UserEntityBuilder {

		private Long id;
		private Integer rollNumber;
		private String emailId;
		private String password;
		private String firstName;
		private String lastName;
		private UserTypeEnum userType;
		private BranchEntity branch;

		private String middleName;
		private GenderEnum gender;
		private Date birthDate;
		private List<SubjectEntity> subjects;

		public UserEntityBuilder(Long id) {
			super();
			this.id = id;
		}

		public UserEntityBuilder setRollNumber(Integer rollNumber) {
			this.rollNumber = rollNumber;
			return this;
		}

		public UserEntityBuilder setEmailId(String emailId) {
			this.emailId = emailId;
			return this;
		}

		public UserEntityBuilder setPassword(String password) {
			this.password = password;
			return this;
		}

		public UserEntityBuilder setFirstName(String firstName) {
			this.firstName = firstName;
			return this;
		}

		public UserEntityBuilder setLastName(String lastName) {
			this.lastName = lastName;
			return this;
		}

		public UserEntityBuilder setUserType(UserTypeEnum userType) {
			this.userType = userType;
			return this;
		}

		public UserEntityBuilder setBranch(BranchEntity branch) {
			this.branch = branch;
			return this;
		}

		public UserEntityBuilder setMiddleName(String middleName) {
			this.middleName = middleName;
			return this;
		}

		public UserEntityBuilder setGender(GenderEnum gender) {
			this.gender = gender;
			return this;
		}

		public UserEntityBuilder setBirthDate(Date birthDate) {
			this.birthDate = birthDate;
			return this;
		}

		public UserEntityBuilder setSubjects(List<SubjectEntity> subjects) {
			this.subjects = subjects;
			return this;
		}

		public UserEntity build() {
			return new UserEntity(this);
		}

	}

	@Override
	public int compareTo(UserEntity o) {
		int bComp = this.getBranch().getName().compareTo(o.getBranch().getName());
		if (bComp != 0) {
			return bComp;
		} else {
			return this.emailId.compareTo(o.emailId);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public BranchEntity getBranch() {
		return branch;
	}

	public void setBranch(BranchEntity branch) {
		this.branch = branch;
	}

	public GenderEnum getGender() {
		return gender;
	}

	public void setGender(GenderEnum gender) {
		this.gender = gender;
	}

	public UserTypeEnum getUserType() {
		return userType;
	}

	public void setUserType(UserTypeEnum userType) {
		this.userType = userType;
	}

	public Integer getRollNumber() {
		return rollNumber;
	}

	public void setRollNumber(Integer rollNumber) {
		this.rollNumber = rollNumber;
	}

	public List<SubjectEntity> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<SubjectEntity> subjects) {
		this.subjects = subjects;
	}

}
