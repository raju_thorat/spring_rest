package com.raju.sp.jpa.rs.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.raju.sp.jpa.rs.entity.InstituteEntity;

public interface InstituteRepository extends JpaRepository<InstituteEntity, Long> {
}