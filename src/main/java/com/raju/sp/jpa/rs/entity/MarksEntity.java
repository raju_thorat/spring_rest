package com.raju.sp.jpa.rs.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "SUBJECT_MARKS")
public class MarksEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Basic(optional = false)
	@Column(name = "ID")
	private Long id;

	@Basic(optional = false)
	@ManyToOne
	@JoinColumn(name = "EXAM_ID", referencedColumnName = "ID", nullable = false)
	private ExamEntity exam;

	@Basic(optional = false)
	@ManyToOne
	@JoinColumn(name = "SUBJECT_ID", referencedColumnName = "ID", nullable = false)
	private SubjectEntity subject;

	@Basic(optional = false)
	@Column(name = "MARKS")
	private Integer marks;

	@Basic(optional = false)
	@ManyToOne
	@JoinColumn(name = "USER_ID", referencedColumnName = "ID", nullable = false)
	private UserEntity user;

	public MarksEntity() {
		super();
	}

	private MarksEntity(MarksEntityBuilder builder) {
		super();
		this.id = builder.id;
		this.exam = builder.exam;
		this.subject = builder.subject;
		this.marks = builder.marks;
		this.user = builder.user;
	}

	public static class MarksEntityBuilder {
		private Long id;
		private ExamEntity exam;
		private SubjectEntity subject;
		private Integer marks;
		private UserEntity user;

		public MarksEntityBuilder(Long id, ExamEntity exam, SubjectEntity subject, Integer marks, UserEntity user) {
			this.id = id;
			this.exam = exam;
			this.subject = subject;
			this.marks = marks;
			this.user = user;
		}

		public MarksEntity build() {
			return new MarksEntity(this);
		}

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getMarks() {
		return marks;
	}

	public void setMarks(Integer marks) {
		this.marks = marks;
	}

	public ExamEntity getExam() {
		return exam;
	}

	public void setExam(ExamEntity exam) {
		this.exam = exam;
	}

	public SubjectEntity getSubject() {
		return subject;
	}

	public void setSubject(SubjectEntity subject) {
		this.subject = subject;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

}
