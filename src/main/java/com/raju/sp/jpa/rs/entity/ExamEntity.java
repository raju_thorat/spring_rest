package com.raju.sp.jpa.rs.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.raju.sp.common.constants.enums.ExamTypeEnum;

@Entity
@Table(name = "exam")
public class ExamEntity implements Serializable, Comparable<ExamEntity> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Basic(optional = false)
	@Column(name = "ID")
	private Long id;

	@Basic(optional = false)
	@Enumerated(EnumType.STRING)
	@Column(name = "EXAM_TYPE", nullable = false)
	private ExamTypeEnum examType;

	@Basic(optional = false)
	@ManyToOne
	@JoinColumn(name = "BRANCH_YEAR_ID", referencedColumnName = "ID", nullable = false)
	private BranchYearEntity branchYear;

	public ExamEntity() {
		super();
	}

	private ExamEntity(ExamEntityBuilder builder) {
		this.id = builder.id;
		this.examType = builder.examType;
		this.branchYear = builder.branchYear;
	}

	public static class ExamEntityBuilder {

		private Long id;
		private ExamTypeEnum examType;
		private BranchYearEntity branchYear;

		public ExamEntityBuilder(Long id) {
			this.id = id;
		}

		public ExamEntityBuilder setExamType(ExamTypeEnum examType) {
			this.examType = examType;
			return this;
		}

		public ExamEntityBuilder setBranchYear(BranchYearEntity branchYear) {
			this.branchYear = branchYear;
			return this;
		}

		public ExamEntity build() {
			return new ExamEntity(this);
		}
	}

	@Override
	public int compareTo(ExamEntity o) {
		return this.id.compareTo(o.id);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ExamTypeEnum getExamType() {
		return examType;
	}

	public void setExamType(ExamTypeEnum examType) {
		this.examType = examType;
	}

	public BranchYearEntity getBranchYear() {
		return branchYear;
	}

	public void setBranchYear(BranchYearEntity branchYear) {
		this.branchYear = branchYear;
	}

}
