package com.raju.sp.jpa.rs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.raju.sp.common.constants.enums.BranchYearTypeEnum;
import com.raju.sp.common.constants.enums.ExamTypeEnum;
import com.raju.sp.jpa.rs.entity.MarksEntity;

public interface MarksRepository extends JpaRepository<MarksEntity, Long> {

	@Query(value = "SELECT me FROM MarksEntity me WHERE me.user.id = :userId AND me.exam.examType=:examType"
			+ " AND me.exam.branchYear.year =:year AND me.exam.branchYear.branchYearType =:branchYearType")
	List<MarksEntity> getResultForStudent(@Param("userId") Long userId, @Param("year") Integer year,
			@Param("branchYearType") BranchYearTypeEnum branchYearType, @Param("examType") ExamTypeEnum examType);

}
