package com.raju.sp.jpa.rs.entity;

import java.io.Serializable;
import java.util.SortedSet;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "institute")
public class InstituteEntity implements Serializable, Comparable<InstituteEntity> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Basic(optional = false)
	@Column(name = "ID")
	private Long id;

	@Basic(optional = false)
	@Column(name = "NAME", unique = true, nullable = false)
	private String name;

	@OrderBy("id,name")
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "institute")
	private SortedSet<BranchEntity> branches;

	public InstituteEntity() {
		super();
	}

	private InstituteEntity(InstituteEntityBuilder builder) {
		this.id = builder.id;
		this.name = builder.name;
		this.branches = builder.branches;
	}

	public static class InstituteEntityBuilder {
		private Long id;
		private String name;
		private SortedSet<BranchEntity> branches;

		public InstituteEntityBuilder(Long id) {
			this.id = id;
		}

		public InstituteEntityBuilder setName(String name) {
			this.name = name;
			return this; 
		}

		public InstituteEntityBuilder setBranches(SortedSet<BranchEntity> branches) {
			this.branches = branches;
			return this;
		}

		public InstituteEntity build() {
			return new InstituteEntity(this);
		}
	}

	@Override
	public int compareTo(InstituteEntity o) {
		return this.name.compareTo(o.name);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SortedSet<BranchEntity> getBranches() {
		return branches;
	}

	public void setBranches(SortedSet<BranchEntity> branches) {
		this.branches = branches;
	}

}
