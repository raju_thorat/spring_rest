package com.raju.sp.jpa.rs.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.raju.sp.jpa.rs.entity.BranchYearEntity;

public interface BranchYearRepository extends JpaRepository<BranchYearEntity, Long> {
}