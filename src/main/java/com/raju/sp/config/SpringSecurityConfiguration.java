//package com.raju.sp.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//
//@Configuration
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true)
//public class SpringSecurityConfiguration extends WebSecurityConfigurerAdapter {
//
//	// private static final String[] securedUrlList = { "/userCtrl/**", "/index" };
//	private static final String[] securedUrlList = { "/dummySecuredCtrl/**" };
//	private static final String[] allowedUrlList = { "/static_res/**" };
//
//	@Bean
//	@Override
//	public AuthenticationManager authenticationManagerBean() throws Exception {
//		return super.authenticationManagerBean();
//	}
//
//	// To avoid complexity CSRF is disabled but in production this must be enabled
//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
//		http.csrf().disable().authorizeRequests().antMatchers(allowedUrlList).permitAll().antMatchers(securedUrlList)
//				.authenticated().and().httpBasic();
//	}
//
//	/**
//	 * Add DB user service later.
//	 */
//	@Override
//	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.inMemoryAuthentication().withUser("admin").password("admin").authorities("ROLE_ADMIN", "ROLE_USER").and()
//				.withUser("user").password("user").authorities("ROLE_USER");
//	}
//}