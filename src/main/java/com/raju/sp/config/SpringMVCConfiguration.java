package com.raju.sp.config;

import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;

/**
 * MVC Specific Configuration
 * 
 * @author raju
 *
 */

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.raju.sp")
@PropertySources({ @PropertySource(value = "classpath:application.properties", ignoreResourceNotFound = false),
		@PropertySource(value = "classpath:hibernate.properties", ignoreResourceNotFound = true) })
// if same key in both files, second file will 'win'
public class SpringMVCConfiguration extends WebMvcConfigurerAdapter {

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/static_res/**").addResourceLocations("/static_res/");
	}

	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/jsp/");
		viewResolver.setSuffix(".jsp");
		registry.viewResolver(viewResolver);
	}

	/**
	 * Avoid lazy initialization exceptions. If list is lazy then jackson will treat
	 * it as null , It's your responsibility to fill up required lists
	 * 
	 * @return Hibernate4Module
	 */
	private Hibernate5Module hibernate5Module() {
		Hibernate5Module h4m = new Hibernate5Module();
		// h4m.disable(Hibernate4Module.Feature.USE_TRANSIENT_ANNOTATION);
		return h4m;
	}

	/**
	 * create the ObjectMapper with Spring's Jackson2ObjectMapperBuilder and passing
	 * the hibernate4Module to modulesToInstall()
	 * 
	 * @return
	 */
	private MappingJackson2HttpMessageConverter jacksonMessageConverter() {
		Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
		builder.featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS).modulesToInstall(hibernate5Module());
		return new MappingJackson2HttpMessageConverter(builder.build());
	}

	/**
	 * 
	 */
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		converters.add(jacksonMessageConverter());
		super.configureMessageConverters(converters);
	}
}