package com.raju.sp.config;

import static com.raju.sp.common.constants.CommonConstants.FORMAT_SQL;
import static com.raju.sp.common.constants.CommonConstants.HBM2DDL;
import static com.raju.sp.common.constants.CommonConstants.HB_DIALECT;
import static com.raju.sp.common.constants.CommonConstants.R_SYSTEM_DATA_SOURCE;
import static com.raju.sp.common.constants.CommonConstants.R_SYSTEM_ENTITY_PKG;
import static com.raju.sp.common.constants.CommonConstants.R_SYSTEM_EN_MGR_FACTORY;
import static com.raju.sp.common.constants.CommonConstants.R_SYSTEM_JDBC_TEMPLATE;
import static com.raju.sp.common.constants.CommonConstants.R_SYSTEM_JNDI;
import static com.raju.sp.common.constants.CommonConstants.R_SYSTEM_SPRING_DATA_REPO_PKG;
import static com.raju.sp.common.constants.CommonConstants.R_SYSTEM_TRAN_MGR;
import static com.raju.sp.common.constants.CommonConstants.SHOW_COMMENT_IN_SQL;
import static com.raju.sp.common.constants.CommonConstants.SHOW_SQL;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * JPA Specific configuration
 * 
 * @author raju
 *
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = R_SYSTEM_SPRING_DATA_REPO_PKG, entityManagerFactoryRef = R_SYSTEM_EN_MGR_FACTORY, transactionManagerRef = R_SYSTEM_TRAN_MGR)
public class ResultSystemJPAConfiguration {

	public static int resultSystemDataSourceCounter = 0;

	// private static final Logger logger = LoggerFactory.getLogger(JPAConfiguration.class);

	@Bean(name = R_SYSTEM_TRAN_MGR)
	public PlatformTransactionManager resultSystemTransactionManager(Environment env) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(resultSystemEntityManagerFactory(env).getObject());
		return transactionManager;
	}

	@Bean(name = R_SYSTEM_JDBC_TEMPLATE)
	public NamedParameterJdbcTemplate resultSystemJdbcTemplate() {
		return new NamedParameterJdbcTemplate(resultSystemDataSource());
	}

	@Bean(destroyMethod = "close", name = R_SYSTEM_DATA_SOURCE)
	public DataSource resultSystemDataSource() {
		JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
		DataSource dataSource = dataSourceLookup.getDataSource(R_SYSTEM_JNDI);
		resultSystemDataSourceCounter++;
		System.err.println("resultSystemDataSourceCounter=" + resultSystemDataSourceCounter);
		if (resultSystemDataSourceCounter > 1) {
			System.exit(1);
		}
		return dataSource;
	}

	@Bean(name = R_SYSTEM_EN_MGR_FACTORY)
	public LocalContainerEntityManagerFactoryBean resultSystemEntityManagerFactory(Environment env) {

		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactoryBean.setDataSource(resultSystemDataSource());
		entityManagerFactoryBean.setPackagesToScan(new String[] { R_SYSTEM_ENTITY_PKG });
		entityManagerFactoryBean.setLoadTimeWeaver(new InstrumentationLoadTimeWeaver());
		entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());

		Map<String, Object> jpaProperties = new HashMap<String, Object>();
		jpaProperties.put(HBM2DDL, env.getRequiredProperty(HBM2DDL));
		jpaProperties.put(SHOW_SQL, env.getRequiredProperty(SHOW_SQL));
		jpaProperties.put(FORMAT_SQL, env.getRequiredProperty(FORMAT_SQL));
		jpaProperties.put(SHOW_COMMENT_IN_SQL, env.getRequiredProperty(SHOW_COMMENT_IN_SQL));
		jpaProperties.put(HB_DIALECT, env.getRequiredProperty(HB_DIALECT));

		entityManagerFactoryBean.setJpaPropertyMap(jpaProperties);

		return entityManagerFactoryBean;
	}

}
