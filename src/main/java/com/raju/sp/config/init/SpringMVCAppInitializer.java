package com.raju.sp.config.init;

import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.raju.sp.config.ResultSystemJPAConfiguration;
import com.raju.sp.config.SpringMVCConfiguration;
//import com.raju.sp.config.SpringSecurityConfiguration;

/**
 * Avoiding web.xml and configuring dispater servlet
 * 
 * @author raju
 *
 */
@Order(1)
public class SpringMVCAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

	@Override
	protected Class<?>[] getRootConfigClasses() {
		// return new Class[] { SpringMVCConfiguration.class,
		// ResultSystemJPAConfiguration.class, SpringSecurityConfiguration.class };
		return new Class[] { SpringMVCConfiguration.class, ResultSystemJPAConfiguration.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return null;
	}

}