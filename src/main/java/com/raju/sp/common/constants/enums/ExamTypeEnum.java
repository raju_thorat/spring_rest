package com.raju.sp.common.constants.enums;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum ExamTypeEnum {

	SEMISTER("Semister"), ANNUAL("Annual");

	private static Map<String, ExamTypeEnum> FORMAT_MAP = Stream.of(ExamTypeEnum.values())
			.collect(Collectors.toMap(s -> s.name, Function.identity()));

	@JsonCreator // This is the factory method and must be static
	public static ExamTypeEnum fromString(String string) {
		return Optional.ofNullable(FORMAT_MAP.get(string)).orElseThrow(() -> new IllegalArgumentException(string));
	}

	private final String name;

	private ExamTypeEnum(String s) {
		name = s;
	}

	public boolean equalsName(String otherName) {
		return name.equals(otherName);
	}

	public String toString() {
		return this.name;
	}
}