package com.raju.sp.common.constants.enums;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum UserTypeEnum {

		STUDENT("Student"), TEACHER("Teacher");

		private static Map<String, UserTypeEnum> FORMAT_MAP = Stream.of(UserTypeEnum.values())
				.collect(Collectors.toMap(s -> s.name, Function.identity()));

		@JsonCreator // This is the factory method and must be static
		public static UserTypeEnum fromString(String string) {
			return Optional.ofNullable(FORMAT_MAP.get(string)).orElseThrow(() -> new IllegalArgumentException(string));
		}

		private final String name;

		private UserTypeEnum(String s) {
			name = s;
		}

		public boolean equalsName(String otherName) {
			return name.equals(otherName);
		}

		public String toString() {
			return this.name;
		}
	}