package com.raju.sp.common.constants;

/**
 * This class will contain commonly used constants in the application
 * 
 * @author rajthora
 *
 */
public class CommonConstants {

	public static final String HBM2DDL = "hibernate.hbm2ddl.auto";
	public static final String SHOW_SQL = "hibernate.show_sql";
	public static final String FORMAT_SQL = "hibernate.format_sql";
	public static final String SHOW_COMMENT_IN_SQL = "hibernate.use_sql_comments";
	public static final String HB_DIALECT = "hibernate.dialect";

	public final static String R_SYSTEM_SPRING_DATA_REPO_PKG = "com.raju.sp.jpa.rs";
	public final static String R_ANALYTICS_SPRING_DATA_REPO_PKG = "com.raju.sp.jpa.ra";

	public static final String R_SYSTEM_ENTITY_PKG = "com.raju.sp.jpa.rs";
	public static final String R_ANALYTICS_ENTITY_PKG = "com.raju.sp.jpa.ra";

	public static final String R_SYSTEM_JNDI = "jdbc/result_system";
	public static final String R_ANALYTICS_JNDI = "jdbc/result_analytics";

	public final static String R_SYSTEM_TRAN_MGR = "resultSystemTransactionManager";
	public final static String R_ANALYTICS_TRAN_MGR = "resultAnalyticsTransactionManager";

	public final static String R_SYSTEM_JDBC_TEMPLATE = "resultSystemJdbcTemplate";
	public final static String R_ANALYTICS_JDBC_TEMPLATE = "resultAnalyticsJdbcTemplate";

	public final static String R_SYSTEM_EN_MGR_FACTORY = "resultSystemEntityManagerFactory";
	public final static String R_ANALYTICS_EN_MGR_FACTORY = "resultAnalyticsEntityManagerFactory";

	public final static String R_SYSTEM_DATA_SOURCE = "resultSystemDataSource";
	public final static String R_ANALYTICS_DATA_SOURCE = "resultAnalyticsDataSource";

}
