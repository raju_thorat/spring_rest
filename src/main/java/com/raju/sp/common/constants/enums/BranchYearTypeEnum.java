package com.raju.sp.common.constants.enums;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum BranchYearTypeEnum {

	FRIST_YEAR("1st Year"), SECOND_YEAR("2nd Year"),THIRD_YEAR("3nd Year");;

	private static Map<String, BranchYearTypeEnum> FORMAT_MAP = Stream.of(BranchYearTypeEnum.values())
			.collect(Collectors.toMap(s -> s.name, Function.identity()));

	@JsonCreator // This is the factory method and must be static
	public static BranchYearTypeEnum fromString(String string) {
		return Optional.ofNullable(FORMAT_MAP.get(string)).orElseThrow(() -> new IllegalArgumentException(string));
	}

	private final String name;

	private BranchYearTypeEnum(String s) {
		name = s;
	}

	public boolean equalsName(String otherName) {
		return name.equals(otherName);
	}

	public String toString() {
		return this.name;
	}
}