<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<html>
<head>
	<title>AngularJS $http Example</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="static_res/lib/css/bootstrap3.3.6.min.css">
	<link rel="stylesheet" href="static_res/css/app.css"></link>
</head>

<body data-ng-app="resultSystemApp" class="ng-cloak">

	<!-- Fixed navbar -->
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
					aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Project name</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#/">Home</a></li>
					<li><a href="#user">Users</a></li>
					<li><a href="#contact">Contact</a></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
						aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span>
					</a>
						<ul class="dropdown-menu">
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else here</a></li>
							<li role="separator" class="divider"></li>
							<li class="dropdown-header">Nav header</li>
							<li><a href="#">Separated link</a></li>
							<li><a href="#">One more separated link</a></li>
						</ul></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>

	<!-- Begin page content -->
	<div class="container">
		<div data-ng-view></div>
	</div>

	<footer class="footer">
		<div class="container">
			<p class="text-muted">Place sticky footer content here.</p>
		</div>
	</footer>

	<!-- Lib JS -->
	<script src="static_res/lib/angular_1.5.8/angular.js"></script>
	<script src="static_res/lib/angular_1.5.8/angular-route.js"></script>

	<script src="static_res/lib/jquery-2.1.4.min.js"></script>
	<!-- Custom JS -->
	<script src="static_res/js/app.js"></script>

	<script src="static_res/js/controllers/main_controller.js"></script>
	<script src="static_res/js/controllers/home_controller.js"></script>
	<script src="static_res/js/controllers/user_controller.js"></script>

	<script src="static_res/js/services/user_service.js"></script>

	<script src="static_res/js/filters.js"></script>
	<script src="static_res/js/directives.js"></script>
</body>
</html>