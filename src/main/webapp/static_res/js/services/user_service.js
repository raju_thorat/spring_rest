'use strict';

var resultSystemServices = angular.module('resultSystemServices', []);
// TODO Create ajax call factory and user .service here
resultSystemServices.factory('userService', function($http, $q) {
	return {
		fetchAllUsers : function() {

			return $http({
				method : 'GET',
				url : 'users'
			}).then(function successCallback(response) {
				return response.data;
			}, function errorCallback(errResponse) {
				return $q.reject(errResponse);
			});
		},

		createUser : function(user) {
			return $http({
				method : 'POST',
				url : 'userCtrl/addOneUser',
				data : user
			}).then(function successCallback(response) {
				return response.data;
			}, function errorCallback(errResponse) {
				return $q.reject(errResponse);
			});
		},

		updateUser : function(user, id) {
			return $http({
				method : 'PUT',
				url : 'userCtrl/updateOneUser/' + id,
				data : user
			}).then(function successCallback(response) {
				return response.data;
			}, function errorCallback(errResponse) {
				return $q.reject(errResponse);
			});
		},

		deleteUser : function(id) {
			return $http({
				method : 'DELETE',
				url : 'userCtrl/deleteOneUser/' + id
			}).then(function successCallback(response) {
				return response.data;
			}, function errorCallback(errResponse) {
				return $q.reject(errResponse);
			});
		}

	};

});
