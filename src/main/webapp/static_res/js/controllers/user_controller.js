'use strict';

resultSystemControllers.controller('userController', function($scope, userService) {
	$scope.user = {
		id : null,
		emailId : '',
		firstName : '',
		lastName : ''
	};

	$scope.users = [];

	$scope.filteredUsers = [];

	$scope.fetchAllUsers = function() {
		userService.fetchAllUsers().then(function(d) {
			$scope.users = $scope.filteredUsers = d;
		}, function(errResponse) {
			console.error('Error while fetching Currencies');
		});
	};
	console.log($scope.users);

	$scope.createUser = function(user) {
		userService.createUser(user).then($scope.fetchAllUsers, function(errResponse) {
			console.error('Error while creating User.');
		});
	};

	$scope.updateUser = function(user, id) {
		userService.updateUser(user, id).then($scope.fetchAllUsers, function(errResponse) {
			console.error('Error while updating User.');
		});
	};

	$scope.deleteUser = function(id) {
		userService.deleteUser(id).then($scope.fetchAllUsers, function(errResponse) {
			console.error('Error while deleting User.');
		});
	};

	$scope.fetchAllUsers();

	$scope.submit = function() {
		if ($scope.user.id == null) {
			console.log('Saving New User', $scope.user);
			$scope.createUser($scope.user);
		} else {
			$scope.updateUser($scope.user, $scope.user.id);
			console.log('User updated with id ', $scope.user.id);
		}
		$scope.reset();
	};

	$scope.edit = function(id) {
		console.log('id to be edited', id);
		for (var i = 0; i < $scope.users.length; i++) {
			if ($scope.users[i].id == id) {
				$scope.user = angular.copy($scope.users[i]);
				break;
			}
		}
	};

	$scope.remove = function(id) {
		console.log('id to be deleted', id);
		for (var i = 0; i < $scope.users.length; i++) {
			if ($scope.users[i].id == id) {
				$scope.reset();
				break;
			}
		}
		$scope.deleteUser(id);
	};

	$scope.reset = function() {
		$scope.user = {
			id : null,
			username : '',
			address : '',
			email : ''
		};
		$scope.myForm.$setPristine(); // reset Form
	};
	$scope.checkBoxChecked = function() {
		alert($scope.checkBoxItem);
	};

	$scope.getFilterUsers = function() {
		if (!$scope.query) {
			$scope.filteredUsers = $scope.users;
		}
		$scope.filteredUsers = [];
		for ( var i in $scope.users) {
			var user = $scope.users[i];
			if (user.firstName.toLowerCase().indexOf($scope.query.toLowerCase()) != -1
					|| user.lastName.toLowerCase().indexOf($scope.query.toLowerCase()) != -1) {
				$scope.filteredUsers.push(user);
			}
		}
	}
});
