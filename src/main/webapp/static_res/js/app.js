'use strict';

var resultSystemApp = angular.module('resultSystemApp', [ 'ngRoute', 'resultSystemControllers', 'resultSystemServices', 'resultSystemFilters' ]);

var resultSystemControllers = angular.module('resultSystemControllers', []);

resultSystemApp.config(function($routeProvider) {
	$routeProvider.when('/', {
		templateUrl : 'static_res/html/home.html',
		controller : 'homeController'
	}).when('/login', {
		templateUrl : 'static_res/html/login.html',
		controller : 'loginController'
	}).when('/user', {
		templateUrl : 'static_res/html/user.html',
		controller : 'userController'
	}).otherwise({
		redirectTo : '/'
	});
});

setTimeout(function() {
	console.log("hello");
}, 4000);