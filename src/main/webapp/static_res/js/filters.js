'use strict';
var resultSystemFilters = angular.module('resultSystemFilters', []);

resultSystemFilters.filter("gender", function() {
	return function(gender) {
		switch (gender) {
		case 'Male':
			return 1;
		case 'Female':
			return 2;
		case 'NotDisclosed':
			return 3;
		}
	}
})