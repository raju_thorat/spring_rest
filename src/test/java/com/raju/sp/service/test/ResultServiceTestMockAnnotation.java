package com.raju.sp.service.test;

import static com.raju.sp.common.constants.enums.BranchYearTypeEnum.FRIST_YEAR;
import static com.raju.sp.common.constants.enums.ExamTypeEnum.ANNUAL;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.Test;

import com.raju.sp.jpa.rs.entity.MarksEntity;
import com.raju.sp.jpa.rs.repository.MarksRepository;
import com.raju.sp.service.ResultService;

public class ResultServiceTestMockAnnotation {

	@Test
	public void getResultForStudent() {
		MarksRepository marksRepository = mock(MarksRepository.class);
		Long anyLong = anyLong();
		Integer anyInt = anyInt();
		when(marksRepository.getResultForStudent(anyLong(), anyInt(), FRIST_YEAR, ANNUAL)).thenReturn(new ArrayList<>());
		ResultService resultService = new ResultService(marksRepository);
		List<MarksEntity> marksList = resultService.getResultForStudent(anyLong, anyInt, FRIST_YEAR.toString(), ANNUAL.toString());
		assertEquals(0, marksList.size());
	}
}
